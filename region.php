<?php

include('auth.php');

// INITIALIZE HTML CREATION & START BODY
Shell::createHeader();
Shell::startBody();
Shell::createTop();

// MAIN WRAPPER START
Shell::startMain('Texas Dashboard');

?>

<div class="row">
    <div class="col">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Texas Production & Sales</h5>
                <canvas id="region"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
      <div class="main-card mb-3 card">
          <div class="card-body"><h5 class="card-title">Oil Production</h5>
              <table class="mb-0 table">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Prod (BBLs)</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
    <div class="col">
      <div class="main-card mb-3 card">
          <div class="card-body"><h5 class="card-title">Oil Production</h5>
              <table class="mb-0 table">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Prod (BBLs)</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
    <div class="col">
      <div class="main-card mb-3 card">
          <div class="card-body"><h5 class="card-title">Oil Production</h5>
              <table class="mb-0 table">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Prod (BBLs)</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
</div>

<?php
// MAIN WRAPPER END
Shell::closeMain();

// END BODY
Shell::closeBody();
?>
