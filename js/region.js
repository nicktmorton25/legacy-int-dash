$(document).ready(function(){

  $.ajax({
    type: 'POST',
    url: 'ajax/production.php',
    data: { level: 'region'}
  }).then((response)=>{

    response = JSON.parse(response);

    var ctx = document.getElementById('region').getContext('2d');
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
  				labels: ['04/01/20', '04/02/20', '04/03/20', '04/04/20', '04/05/20', '04/06/20', '04/07/20'],
  				datasets: [{
  					label: 'Oil Production',
  					backgroundColor: window.chartColors.green,
  					borderColor: window.chartColors.green,
  					data: response['oil'],
            fill: false
  				}, {
  					label: 'Water Production',
  					fill: false,
  					backgroundColor: window.chartColors.blue,
  					borderColor: window.chartColors.blue,
  					data: response['water'],
            fill: false
  				}, {
  					label: 'Gas Production',
  					fill: false,
  					backgroundColor: window.chartColors.red,
  					borderColor: window.chartColors.red,
  					data: response['gas'],
            fill: false
  				}]
  			},
  			options: {
  				responsive: true,
  				title: {
  					display: false,
  					text: ''
  				},
  				tooltips: {
  					mode: 'index',
  					intersect: false,
  				},
  				hover: {
  					mode: 'nearest',
  					intersect: true
  				},
  				scales: {
  					xAxes: [{
  						display: true,
  						scaleLabel: {
  							display: true,
  							labelString: 'Date'
  						}
  					}],
  					yAxes: [{
  						display: true,
  						scaleLabel: {
  							display: true,
  							labelString: 'Value (BBL)'
  						}
  					}]
  				}
  			}
    });
  });

});
