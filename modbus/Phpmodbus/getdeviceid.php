<?php

include("/var/www/html/functions.php");

$db=connect("account_43");

$stmt=$db->query("SELECT * FROM tbl_modbus_mapping WHERE map_friendly_name IS NOT NULL");
$results=$stmt->fetchAll(PDO::FETCH_ASSOC);

foreach($results as $row){
    
    $opcConv = $row['map_opcua_convention'];
    $opcConv = explode("_",$opcConv);
    
    $device = $opcConv[3];
    $device = str_replace("Device","",$device);
    
    $stmt=$db->prepare("UPDATE tbl_modbus_mapping SET map_device_id = ? WHERE mapID = ?");
    $stmt->execute(array($device,$row['mapID']));
}

?>
