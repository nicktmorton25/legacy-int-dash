<?php

include("/var/www/html/functions.php");

$db = connect("account_46");
$stmt=$db->query("SELECT * FROM tbl_modbus_mapping_v2 WHERE map_siteID = 10");
$results=$stmt->fetchAll(PDO::FETCH_ASSOC);

foreach($results as $row){
    
    $tagArray = explode(".",$row['map_tag_name']);
    $tagArray[0] = "OLDHAM_3871";
    $tagName = implode(".",$tagArray);

    
    $stmt=$db->prepare("INSERT INTO tbl_modbus_mapping_v2 (`map_siteID`,`map_modbus_address`,`map_tag_name`,`map_friendly_name`,`map_array_group`,`map_array_position`,`map_tag_usable`,`map_units`,`map_measurement_factor`,`map_tag_config`) VALUES (?,?,?,?,?,?,?,?,?,?)");
    $stmt->execute(array(11,$row['map_modbus_address'],$tagName,$row['map_friendly_name'],$row['map_array_group'],$row['map_array_position'],$row['map_tag_usable'],$row['map_units'],$row['map_measurement_factor'],$row['map_tag_config']));
}


?>