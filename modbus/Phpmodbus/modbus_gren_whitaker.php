<?php

// Display all errors
error_reporting(E_ALL);
ini_set("display_startup_errors",1);
ini_set("display_errors",1);
ini_set('max_execution_time', -1);
ini_set('memory_limit',-1);

// Include Files
require_once 'ModbusMaster.php';
include("../../auth.php");

$results = Readings::getMappings(1);

$modbus = new ModbusMaster("107.241.200.74", "TCP", "5502");

foreach($results as $row){
    $start_addr = $row['addr'] - 400000;
    try {
        $recData = $modbus->readMultipleRegisters(1, $start_addr - 1, 2);
        // Convert to 32-bit UINT
        $values = array_chunk($recData, 4);
        $i = 0;
        foreach($values as $bytes) {
            $mb_data[$i] = PhpType::bytes2float($bytes);
            $i++;
        }
        $array[$row['id']] = round($mb_data[0],2);
    } catch (Exception $e) {
        echo "MB Err: " . $modbus . $e . "\n";
    }
}

if(sizeof($array) == 0){ exit(); }

$query = "INSERT INTO readings (`map_id`,`value`) VALUES ";
$i=0;
foreach($array as $mapID => $reading){
    if($reading >= 0){
      if($i > 0){ $query .= ","; }
      $query .= "($mapID,$reading)";
      $i++;
    }
}

echo $query; exit();

?>
