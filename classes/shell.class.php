<?php

class Shell{

  public static function createHeader(){
    echo '<!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Analytics Dashboard - This is an example dashboard created using build-in elements and components.</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
        <meta name="description" content="This is an example dashboard created using build-in elements and components.">
        <meta name="msapplication-tap-highlight" content="no">
        <!--
        =========================================================
        * ArchitectUI HTML Theme Dashboard - v1.0.0
        =========================================================
        * Product Page: https://dashboardpack.com
        * Copyright 2019 DashboardPack (https://dashboardpack.com)
        * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
        =========================================================
        * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
        -->
        <link href="./main.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    </head>';
  }

  public static function startBody(){
    echo '<body>
        <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">';
  }

  public static function createTop(){
    echo '<div class="app-header header-shadow">
        <div class="app-header__logo">
            <div><h5 class="mt-2">Grenadier Energy</h5></div>
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>';
  }

  public static function startMain($title){
    echo '<div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>    <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">

                            <li class="app-sidebar__heading">Overview</li>
                            <li>
                                <a href="index.php"><i class="metismenu-icon pe-7s-global"></i>Global</a>
                            </li>
                            <li>
                                <a href="region.php"><i class="metismenu-icon pe-7s-map-2"></i>Texas</a>
                            </li>

                            <li class="app-sidebar__heading">Sites</li>
                            <li>
                              <a>
                                <i class="metismenu-icon pe-7s-map-marker"></i>
                                Wright Battery
                                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                              </a>
                              <ul>
                                <li>
                                  <a href="site.php?id=1">
                                    <i class="metismenu-icon"></i>
                                    View Site
                                  </a>
                                </li>
                                <hr>
                                <li>
                                  <a href="">
                                    <i class="metismenu-icon"></i>
                                    Oil Tank 1
                                  </a>
                                </li>
                                <li>
                                  <a href="">
                                    <i class="metismenu-icon"></i>
                                    Oil Tank 2
                                  </a>
                                </li>
                                <li>
                                  <a href="">
                                    <i class="metismenu-icon"></i>
                                    Oil Tank 3
                                  </a>
                                </li>
                              </ul>
                            </li>
                            <li>
                              <a>
                                <i class="metismenu-icon pe-7s-map-marker"></i>
                                Whitaker
                                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                              </a>
                              <ul>
                                <li>
                                  <a href="elements-buttons-standard.html">
                                    <i class="metismenu-icon"></i>
                                    Oil Tank 1
                                  </a>
                                </li>
                                <li>
                                  <a href="elements-dropdowns.html">
                                    <i class="metismenu-icon"></i>
                                    Oil Tank 2
                                  </a>
                                </li>
                                <li>
                                  <a href="elements-icons.html">
                                    <i class="metismenu-icon"></i>
                                    Oil Tank 3
                                  </a>
                                </li>
                              </ul>
                            </li>

                            <li class="app-sidebar__heading">Alarms</li>
                            <li>
                                <a href="index.html"><i class="metismenu-icon pe-7s-info"></i>Overview</a>
                            </li>
                            <li>
                                <a href="index.html"><i class="metismenu-icon pe-7s-stopwatch"></i>Recent</a>
                            </li>

                            <li class="app-sidebar__heading">Admin</li>
                            <li>
                                <a href="index.html"><i class="metismenu-icon pe-7s-users"></i>Users</a>
                            </li>
                            <li>
                                <a href="index.html"><i class="metismenu-icon pe-7s-config"></i>Settings</a>
                            </li>

                            <li class="app-sidebar__heading">Profile</li>
                            <li>
                                <a href="index.html"><i class="metismenu-icon pe-7s-user"></i>Account</a>
                            </li>
                            <li>
                                <a href="index.html"><i class="metismenu-icon pe-7s-lock"></i>Password</a>
                            </li>
                            <li>
                                <a href="index.html"><i class="metismenu-icon pe-7s-back-2"></i>Logout</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div>'.$title.'</div>
                            </div>
                          </div>
                    </div>';
  }

  public static function closeMain(){
    echo '</div>
      </div>';
  }

  public static function closeBody(){
    echo '</div>
      <script type="text/javascript" src="./assets/scripts/main.js"></script></body>
      <script type="text/javascript" src="js/global.js"></script></body>
      <script type="text/javascript" src="js/region.js"></script></body>
      <script type="text/javascript" src="js/site.js"></script></body>
      </html>';
  }


}

?>
