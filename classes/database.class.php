<?php

class Database {

  private static function setupConn($admin){
    $host_name = Configuration::DB_HOSTNAME;
    //$database = $admin ? 'admin' : 'account_'.Account::$accountID;
    $database = $admin ? 'admin' : 'account_1';
  	$username = Configuration::DB_USERNAME;
  	$password = Configuration::DB_PASSWORD;
  	try {
  		$db = new PDO('mysql:host='.$host_name.';dbname='.$database, $username, $password);
  	} catch (PDOException $e) {
  		print "Error!: " . $e->getMessage() . "<br/>";
  		die();
  	}
  	return $db;
  }

  public static function runQuery($admin,$query,$return=false,$lastid=false){
    $db = self::setupConn($admin);
    $stmt = $db->query($query);
    if(!$lastid){
      if($return){
        $results = $return > 1 ? $stmt->fetchAll(PDO::FETCH_ASSOC) : $stmt->fetch(PDO::FETCH_ASSOC);
        return $results;
      }
    }else{
      return $db->lastInsertId();
    }
  }

  public static function prepareInsertStatement($table,$data){
    $keystr = "";
    $valstr = "";
    $i=0;
    foreach($data as $k=>$v){
      if($v != ''){
          if($i > 0){ $keystr .= ","; $valstr .= ","; }
          $keystr .= '`'.$k.'`';
          $valstr .= "'".$v."'";
      }
      $i++;
    }
    $statement = "INSERT INTO $table ( $keystr ) VALUES ( $valstr )";
    return $statement;
  }

  public static function prepareUpdateStatement($table,$data,$id){
    $updstr = "";
    $i=0;
    foreach($data as $k=>$v){
      if($v != ''){
          if($i > 0){ $updstr .= ","; }
          $updstr .= '`'.$k.'` = '."'".$v."'";
          $i++;
      }
    }
    $statement = "UPDATE $table SET $updstr WHERE id = $id";
    return $statement;
  }

}

?>
