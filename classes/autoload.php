<?php
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
header('Expires: 0');
function autoload($className)
{
	$filename = preg_replace('/([A-Z])/', '_$1', $className);
	$filename = realpath(__DIR__.'/'.substr(strtolower($filename), 1).'.class.php');
	if ((file_exists($filename) === false) || (is_readable($filename) === false)) {
		return false;
	}
	require_once $filename;
}
spl_autoload_register('autoload');
?>
