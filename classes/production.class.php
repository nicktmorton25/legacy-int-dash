<?php

class Production{

  private $level = Null;
  private $id = Null;

  public function __construct($level,$id=Null){
    $this->level = $level;
    $this->id = $id;
  }

  public function getProduction(){
    $production = Null;
    switch($this->level){
      case 'global':
        $production = $this->getGlobalProduction();
        break;
      case 'region':
        $production = $this->getRegionalProduction();
        break;
      case 'site':
        $production = $this->getSiteProduction();
        break;
    }
    return $production;
  }

  private function getGlobalProduction(){
    return json_encode(array(
      'oil' => array(1.0,2.0,3.0,4.0,5.0,6.0,7.0),
      'water' => array(7.0,6.0,5.0,4.0,3.0,2.0,1.0),
      'gas' => array(3.0,7.0,5.0,8.0,3.0,2.0,4.0)
    ));
  }

  private function getRegionalProduction(){
    return json_encode(array(
      'oil' => array(1.0,2.0,3.0,4.0,5.0,6.0,7.0),
      'water' => array(7.0,6.0,5.0,4.0,3.0,2.0,1.0),
      'gas' => array(3.0,7.0,5.0,8.0,3.0,2.0,4.0)
    ));
  }

  private function getSiteProduction(){
    return json_encode(array(
      'oil' => array(1.0,2.0,3.0,4.0,5.0,6.0,7.0),
      'water' => array(7.0,6.0,5.0,4.0,3.0,2.0,1.0),
      'gas' => array(3.0,7.0,5.0,8.0,3.0,2.0,4.0)
    ));
  }

}

?>
