<?php

include('auth.php');

// INITIALIZE HTML CREATION & START BODY
Shell::createHeader();
Shell::startBody();
Shell::createTop();

// MAIN WRAPPER START
Shell::startMain('Tag Manager');

$siteID = $_GET['id'];

?>
<ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
    <li class="nav-item">
        <a role="tab" class="nav-link" href="site.php?id=<?php echo $siteID; ?>">
            <span>Production Overview</span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link active" href="tags.php?id=<?php echo $siteID; ?>">
            <span>Tag Manager</span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link" href="#tab-content-1">
            <span>Site Alarms</span>
        </a>
    </li>
</ul>
<div class="row">
    <div class="col">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Site Tag Manager</h5>
                <canvas id="site"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
      <div class="main-card mb-3 card">
          <div class="card-body"><h5 class="card-title">Oil Production</h5>
              <table class="mb-0 table">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Prod (BBLs)</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  <tr>
                    <th scope="row">04/01/2020</th>
                    <td>130.94</td>
                  </tr>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
</div>

<?php
// MAIN WRAPPER END
Shell::closeMain();

// END BODY
Shell::closeBody();
?>
